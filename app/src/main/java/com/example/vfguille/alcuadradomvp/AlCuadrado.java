package com.example.vfguille.alcuadradomvp;

public interface AlCuadrado {
    interface View{
        void showResult(String result);
        void showError(String error);
    }

    interface Presenter{
        void showResult(String result);
        void alCuadrado(String numero);
        void showError(String error);
    }

    interface Model{
        void alCuadrado(String numero);
    }
}
