package com.example.vfguille.alcuadradomvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class AlCuadradoView extends AppCompatActivity implements AlCuadrado.View {

    private TextView tvResultado;
    private EditText edNumero;
    private AlCuadrado.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edNumero=(EditText)findViewById(R.id.edNumero);
        tvResultado=(TextView)findViewById(R.id.tvResultado);
        presenter=new AlCuadradoPresenter(this);
    }

    public void calcular(View view){
        presenter.alCuadrado(edNumero.getText().toString());
    }

    @Override
    public void showResult(String result) {
        tvResultado.setText("Resultado: " + result);
    }

    @Override
    public void showError(String error) {
        tvResultado.setText(error);
    }
}
