package com.example.vfguille.alcuadradomvp;

import android.view.inputmethod.InputMethodManager;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class AlCuadradoModel implements AlCuadrado.Model{
    private AlCuadrado.Presenter presenter;
    private double resultado;

    public AlCuadradoModel(AlCuadrado.Presenter presenter){
        this.presenter=presenter;
    }

    @Override
    public void alCuadrado(String numero) {
        if (numero.equals(""))
            presenter.showError("Debe introducir un número.");
        else
            resultado=Double.valueOf(numero)*Double.valueOf(numero);
            presenter.showResult(String.valueOf(((int)resultado)));
    }
}
